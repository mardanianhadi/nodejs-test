const { check } = require('express-validator');

class RegisterValidator { 
    handle(){
        return [
            check('email').isEmail().withMessage('Email format is invalid'),
            check('password').not().isEmpty().withMessage('password is required')
        ]
    }
}

module.exports = new RegisterValidator();