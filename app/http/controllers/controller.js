const autoBind = require('auto-bind');
const { validationResult } = require('express-validator');


module.exports = class Controller { 
    constructor(){
        autoBind(this);
    }
    back(req, res){
        res.redircet(req.header('Referer') || '/');
    }
    validationForm(req){
        const result = validationResult(req);
        if(!result.isEmpty()){
            const array = result.array();
            let errors = array.map(item => ({message: item.msg, error: true}));
            
            req.flash('errors', errors)
            return false;
        }
        return true;
    }
}
