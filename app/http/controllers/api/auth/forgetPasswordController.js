const Controller = require('./../../controller');
const ForgetPassword = require('../../../../lib/models/forgetPassword');
const uniqueString = require('unique-string');
const User = require('../../../../lib/models/user');
const nodemailer = require('nodemailer');

class ForgetPasswordController extends Controller { 

    
    async reset(req, res, next){
        let {email} = req.body;
        //send email POST /api/auth/password/reset/:token
        let newForget = new ForgetPassword({
            user: req.user._id,
            token: uniqueString
        })
        await newForget.save();
        let transporter = nodemailer.createTransport({
            host: "smtp.mailtrap.io",
            port: 465,
            secure: false, // true for 465, false for other ports
            auth: {
              user: '4e52a1ac6b930b',
              pass: '86fbceef0fe68c'
            },
          });
        
          // send mail with defined transport object
          let info = await transporter.sendMail({
            from: '"nodejs-test" <foo@example.com>', 
            to:`${email}`, 
            subject: "Reset password", 
            text: "Reset your password via this link, send POST request and send your password in body", 
            html:
            `   
                <h1>reset link</h1>
                <form action="localhost:3000/api/auth/password/reset/${newForget.token}" method="POST">
                    <label for="password">new Password: </label>
                    <input type="text" name="password">
                </form>
            `, 
          });

          transporter.sendMail(info, (err, data) => {
            if(err) console.log(err);
            return res.json('reset link sent to your email');
          });
        
    }
    async resetProcess(req, res, next){
        let {token} = req.params;
        let {passowrd} = req.body;
        let forget = await ForgetPassword.findOne({token});
        if(forget.used){
            return res.json('token is used');
        }
        let user = await User.findById(forget.user);
        if(!user) return res.json('user not found');
        user.password = password;
        await user.save();
        return res.json('new password was set');
    }

}


module.exports = new ForgetPasswordController();