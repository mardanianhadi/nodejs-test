const Controller = require('../../controller');
const passport = require('passport');

class RegisterController extends Controller {
    register(req, res, next){
        const result = this.validationForm(req);
        if(result) this.registerProcess(req, res, next);
        else return res.redirect('/api/fail');
    }
    registerProcess(req, res, next){
        passport.authenticate('local-register', {
            successRedirect: '/api/success',
            failureRedirect: '/api/fail'
        })(req, res, next);
    }
}


module.exports = new RegisterController();