const Controller = require('../../controller');
const passport = require('passport');


class LoginController extends Controller {
    login(req, res, next){
        const result = this.validationForm(req);
        if(result) this.loginProcess(req, res, next);
        else return res.redirect('/');
    }
    loginProcess(req, res, next){
        passport.authenticate('local-login', (err, user) => {
            if(!user){
                req.flash('errors', 'user not found');
                return res.redirect('/api/fail');
            }
            req.login(user, err => {
                if(err) console.log(err);
                req.flash('success', 'you loged in successfully');
                return res.redirect('/api/success');
            });
        })(req, res, next);
    }
    googleLogin(req, res, next){
        passport.authenticate('google', {scope: ['profile', 'email']})(req, res, next);
    }
    googleLoginCallback(req, res, next){
        passport.authenticate('google', {failureRedirect: '/api/fail', successRedirect: '/api/success'})(req, res, next);
    }
}


module.exports = new LoginController();