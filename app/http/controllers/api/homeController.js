const Controller = require('../controller');

class HomeController extends Controller {
    index(req, res, next){
        
        return res.json({
            authenticated: req.isAuthenticated()
        });
    }
    success(req, res, next){
        
        return res.json({
            success: req.flash('success')
        });
    }
    fail(req, res, next){
        
        return res.json({
            errors: req.flash('errors')
        });
    }
}



module.exports = new HomeController();