const config = require('./lib/config/config');
const flash = require('connect-flash');
const passport = require('passport');
const path = require('path');
const mongoose = require('mongoose');
const http = require('http');
const session = require('express-session');
const ConnectMongo = require('connect-mongo')(session);
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();

module.exports = class App {
    constructor(){
        this.configServer();
        this.configDatabase();
        this.setConfig();
        this.setRoutes();
    }
    configServer(){
        const PORT = process.env.PORT || 3000;
        const server = http.createServer(app);
        server.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`);
        });
    }
    configDatabase(){
        mongoose.Promise = global.Promise;
        mongoose.connect(config.statics.connectionString, {
            useNewUrlParser: true, 
            useUnifiedTopology: true
        })
        .then(() => console.log('Connected to mongodb'))
        .catch(err => console.log('Could not connect to mongodb'));
    }
    setConfig(){
    //======================settings=======================
        
    //=======================uses==========================
        require('./lib/passport/passport-local');
        require('./lib/passport/passport-google');
        app.use(express.static(path.resolve('./public')));
        app.use(bodyParser.urlencoded({extended: true}));
        app.use(bodyParser.json());
        app.use(cookieParser(config.statics.session.secret));
        app.use(session({
            resave: true,
            saveUninitialized: true,
            secret: config.statics.session.secret,
            cookie: {
                secure: false
            },
            store: new ConnectMongo({mongooseConnection: mongoose.connection})
        }));
        app.use(passport.initialize());
        app.use(passport.session());
        app.use(flash());
    }
    setRoutes(){
        app.use(require('./routes/index'));
    }
}
