const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/user');
const axios = require('axios').default;
var getIP = require('ipware')().get_ip;


passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use('local-register', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, async(req, email, password, done) => {
    let user = await User.findOne({email});
    if(user) return done(null, false, req.flash('errors', 'A user with this email exist'));
    let json = await axios.get(`http://ip-api.com/json/${req.ip === '::1' ? '127.0.0.1' : req.ip}`);
    
    console.log(json.data);
    user = new User({
        email,
        password,
        lon: json.data.lon, 
        lat: json.data.lat
    });
    try{
        await user.save();
        
    }catch(ex){
        return done(null, false, req.flash('errors', 'some error happend while creating the user'));
    }
    return done(null, user, req.flash('success', 'user created successfully'));
}));

passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, email, password, done) => {
    let user = await User.findOne({email});
    if(!user) return done(null, false, req.flash('errors', 'user not found'));
    if(!user.comparePassword(password)) return done(null, false, req.flash('errors', 'password is wrong'));
    return done(null, user);
}));
