const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const config = require('./../config/config');
const GOOGLE_CLIENT_ID = config.google.googleLogin.cliendId;
const GOOGLE_CLIENT_SECRET = config.google.googleLogin.clientSecret;
const User = require('../models/user');

passport.use(new GoogleStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    callbackURL: 'http://localhost:3000/api/auth/google/callback'
}, async (accessToken, refreshToken, profile, done) => {
    
    let user = null;
    try{
        user = await User.findOne({email: profile.emails[0].value})
        if(user){
            user.googleID = profile.id;
            return done(null, user);
        }else{
            let newUser = new User({
                email: profile.emails[0].value,
                googleId: profile.id,
                profile: profile.photos[0].value
            });
            await newUser.save();
        }

    }catch(ex){
        return done(null, false);
    }
    
    
}))