const mongoose = require('mongoose');


const ForgetPassword = new mongoose.Schema({
    token: {type: String, required: true},
    user: {type: mongoose.Types.ObjectId, ref: 'User'},
    used: {type: Boolean, default: false}
});



module.exports = mongoose.model('ForgetPassword', ForgetPassword);