const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const uniqueString = require('unique-string');
const jwt = require('jsonwebtoken');
const config = require('../config/config');

const User = new mongoose.Schema({
    email: {type: String, required: true},
    password: {type: String, required: true},
    rememberToken: {type: String, default: ''},
    lat: {type: String, default: ''},
    lon: {type: String, default: ''}
});

User.pre('save', function(next){
    let salt = bcrypt.genSaltSync(15);
    let hashed = bcrypt.hashSync(this.password, salt);
    this.password = hashed;
    next();
});

User.methods.comparePassword = function(password){
    return bcrypt.compareSync(password, this.password);
}

User.methods.setJwtToken = function(res){
    const token = jwt.sign({_id: this._id, email: this.email, password: this.password}, config.statics.session.secret);
    res.cookie('nodejsTest_jwt', token, {maxAge: 1000 * 60 * 60 * 24, httpOnly: true, secure: true, signed: true})
}

User.methods.checkJwtToken = function(token){
    let obj = jwt.verify(token, config.statics.session.secret);
    if((obj._id && obj._id == this._id) && 
       (obj.email && obj.email == this.email) && 
       (obj.password && obj.password == this.password)
       ){
           return true;
       }
    return false
}

User.methods.setRememberToken = function(res){
    const token = uniqueString();
    res.cookie('nodejsTest_remember', token, {maxAge: 1000 * 60 * 60 * 24, httpOnly: true, secure: true, signed: true})
    this.rememberToken = token;
}

module.exports = mongoose.model('User', User);