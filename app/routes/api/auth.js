const express = require('express');
const router = express.Router();
//==========================controllers==========================
const registerController = require('../../http/controllers/api/auth/registerController');
const loginController = require('../../http/controllers/api/auth/loginController');
const forgetPasswordController = require('../../http/controllers/api/auth/forgetPasswordController');
//=============================validators=======================
const registerValidator = require('../../http/validators/api/registerValidator');
const loginValidator = require('../../http/validators/api/loginValidator');
router.use((req, res, next) => {
    console.log('ip: ', req.ip);
    next();
})

router.post('/register', registerValidator.handle(), registerController.register);
router.post('/login', loginValidator.handle(), loginController.login);
router.get('/google', loginController.googleLogin);
router.get('/google/callback', loginController.googleLoginCallback);
router.post('/password/reset', forgetPasswordController.reset);
router.post('/password/reset/:token', forgetPasswordController.resetProcess);








module.exports = router;