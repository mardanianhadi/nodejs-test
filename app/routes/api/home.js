const express = require('express');
const router = express.Router();

//===========================controllers=========================
const homeController = require('../../http/controllers/api/homeController');;


router.get('/', homeController.index)
router.get('/fail', homeController.fail);
router.get('/success', homeController.success);



module.exports = router;